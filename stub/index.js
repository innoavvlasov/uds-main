const express = require("express");
const app = express();
const path = require('path')
const proxy = require('http-proxy-middleware');

app.use(express.static("dist"));

app.get(["/uds-main/mainApp.js"], function(request, response) {
    let reqPath = path.join(__dirname, "../");
    response.sendFile(
      path.resolve(reqPath, "/dist/defaultApp.js")
    );
  });
  console.log('process.env', process.env.BH)
if(process.env.BH) {
  app.use(
    '/main',
    proxy({ target: 'http://localhost:8090' })
  );
} else {
  app.use(require('./api'))
}



app.listen(3000, () => console.log("Listening on port 3000!"));

module.exports = app;