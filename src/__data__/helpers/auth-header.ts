export function authHeader(): { 'Authorization'?: string } {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}

export function isAdmin() {
    let user = JSON.parse(localStorage.getItem('user'));

    return user && user.token
}