import React from 'react';

export const AreaIcons = React.lazy(() =>
  import(/* webpackChunkName: "area-icons" */ './area-icons')
    .then(({ AreaIcons }) => ({ default: AreaIcons })),
);

export const Competitions = React.lazy(() =>
  import(/* webpackChunkName: "competitions" */ './competitions')
    .then(({ Competitions }) => ({ default: Competitions })),
);

export const GallarySlider = React.lazy(() =>
  import(/* webpackChunkName: "gallary-slider" */ './gallary-slider')
    .then(({ GallarySlider }) => ({ default: GallarySlider })),
);

// export const MapWithForm = React.lazy(() =>
//   import(/* webpackChunkName: "map-with-form" */ './map-with-form')
//     .then(({ MapWithForm }) => ({ default: MapWithForm })),
// );

export const SocialSection = React.lazy(() =>
  import(/* webpackChunkName: "social" */ './social')
    .then(({ SocialSection }) => ({ default: SocialSection })),
);

export const TopSlider = React.lazy(() =>
  import(/* webpackChunkName: "top-slider" */ './top-slider')
    .then(({ TopSlider }) => ({ default: TopSlider })),
);

export const VideoSection = React.lazy(() =>
  import(/* webpackChunkName: "video-section" */ './video-section')
    .then(({ VideoSection }) => ({ default: VideoSection })),
);