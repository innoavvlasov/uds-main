/**
 * Слайд.
 *
 * @prop {string} title Заголовок.
 * @prop {string} message Текстовка.
 * @prop {any} picture Картинка.
 * @prop {string} alt Alt картинки.
 */
export interface Slide {
    title: string;
    message: string;
    picture: any;
    alt: string;
}

/**
 * Варианты состояния перехода.
 *
 * IDEL - Состояние спокойствия.
 * DESOLVE - Сщстояние исчезновения.
 */
export enum TransitionState {
    IDLE,
    DESOLVE,
}
