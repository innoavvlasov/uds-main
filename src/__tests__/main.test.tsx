import React from 'react';
import moxios from 'moxios';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as Thunk from 'redux-thunk';

configure({ adapter: new Adapter() });

import App from '../';

describe('main moxios', () => {
    beforeEach(() => moxios.install());

    afterEach(() => moxios.uninstall());

    it('testings requests', (done) => {
        const element = mount(
            <Provider store={createStore(combineReducers({ app: App['reducer'] }), applyMiddleware(Thunk.default))}>
                <App />
            </Provider>
        );

        /**
         * Первоначальное состояние со скелетоном.
         */
        expect(element.html())
                .toMatchSnapshot();

        moxios.wait(async () => {
            element.update()
            const request = moxios.requests.at(0);

            await request.respondWith({
                status: 200,
                response: require('../../stub/api/getMainData.json')
            })

            element.update()
            /**
             * Отрисовываем данные
             */
            expect(element.html())
                .toMatchSnapshot()
            done()
        })
    })

    it('testings requests', (done) => {
        const element = mount(
            <Provider store={createStore(combineReducers({ app: App['reducer'] }), applyMiddleware(Thunk.default))}>
                <App />
            </Provider>
        );

        /**
         * Первоначальное состояние со скелетоном.
         */
        expect(element.html())
                .toMatchSnapshot();

        moxios.wait(async () => {
            element.update()
            const request = moxios.requests.at(0);

            await request.respondWith({
                status: 404,
                response: 'Not found'
            })

            element.update()
            /**
             * Отрисовываем данные
             */
            expect(element.html())
                .toMatchSnapshot()
            done()
        })
    })
})