module.exports = {
    presets: [
        ['@babel/preset-env', {targets: {node: 'current'}}],
        '@babel/preset-typescript',
        '@babel/preset-react',
    ],
    "plugins": [
        "transform-class-properties",
        "@babel/plugin-proposal-optional-chaining",
        ["module-resolver", {
            "root": ["./src"],
            "alias": {
                "@main/components": "./src/components",
                "@main/__data__": "./src/__data__",
                "@main": "./src"
            }
        }]
    ]
};
