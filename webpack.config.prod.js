const webpackCopy = require("copy-webpack-plugin");
const webpack = require("webpack");
const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const { TsConfigPathsPlugin } = require("awesome-typescript-loader");

const outputDirectory = "dist";

module.exports = {
  entry: {
    mainApp: ["./src/index.tsx"]
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, outputDirectory),
    libraryTarget: "umd",
    chunkFilename: '[name].js',
    globalObject: `(typeof self !== 'undefined' ? self : this)`,
    publicPath: "/uds-main/"
  },
  node: {
    fs: "empty"
  },
  plugins: [
    new CleanWebpackPlugin([outputDirectory]),
    new webpack.DefinePlugin({
      "typeof window": JSON.stringify("object")
    }),
    new webpackCopy([{ from: "./static/", to: "static" }])
  ],
  // devtool: '#source-map',
  devtool: "none",
  resolve: {
    modules: ["node_modules", "src"],
    extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"],
    plugins: [new TsConfigPathsPlugin()]
  },
  module: {
    rules: [
      { parser: { system: false } },
      {
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader"
      },
      {
        test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/,
        loader: "file-loader"
      }
    ]
  },
  externals: {
    react: "react",
    "react-dom": "react-dom",
    redux: "redux",
    "react-redux": "react-redux",
    "styled-components": "styled-components"
  }
};
